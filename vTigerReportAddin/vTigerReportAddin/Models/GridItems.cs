﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vTigerReportAddin.Models
{
    public class GridItems
    {
        public string Mergefield { get; set; }
        public string vTigerfield { get; set; }
        public string vAccount { get; set; }
    }
}
