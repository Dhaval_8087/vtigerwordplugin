﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace vTigerReportAddin.Models
{
    public class CultureChange
    {
        ResourceManager res_man;    // declare Resource manager to access to specific cultureinfo
        CultureInfo cul;
        public CultureChange(string formName, string Language)
        {
            res_man = new ResourceManager("vTigerReportAddin.CultureResource." + formName, typeof(CultureChange).Assembly);
            switch_language(Language);
        }
        private void switch_language(string Language)
        {
            switch (Language)
            {
                case "vi":
                    cul = CultureInfo.CreateSpecificCulture("vi");
                    break;
                case "en":
                    cul = CultureInfo.CreateSpecificCulture("en");
                    break;
                case "fr":
                    cul = CultureInfo.CreateSpecificCulture("fr");
                    break;
                case "de-DE":
                    cul = CultureInfo.CreateSpecificCulture("de-DE");
                    break;
                default:
                    cul = CultureInfo.CreateSpecificCulture("en-US");
                    break;
            }
        }
       public string getValue(string LabelValue)
        {
            return res_man.GetString(LabelValue, cul);
        }
    }
}
