﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vTigerReportAddin.Models
{
    public class BusinessClass
    {
        public BusinessClass()
        {
            ComparisionCollection = new List<Comparision>();
            ComparisionCollection.Add(new Comparision() { Name = "equal to", Value = 0 });
            ComparisionCollection.Add(new Comparision() { Name = "unequal to", Value = 1 });
            ComparisionCollection.Add(new Comparision() { Name = "greater than or equal to", Value = 2 });
            ComparisionCollection.Add(new Comparision() { Name = "greater than", Value = 3 });
            ComparisionCollection.Add(new Comparision() { Name = "less than or equal to", Value = 4 });
            ComparisionCollection.Add(new Comparision() { Name = "less than", Value = 5 });
            ComparisionCollection.Add(new Comparision() { Name = "empty", Value = 6 });
            ComparisionCollection.Add(new Comparision() { Name = "not empty", Value = 7 });
        }

        private DataTable _OrignalReportData=new DataTable();

        public DataTable OrignalReportData
        {
            get { return _OrignalReportData; }
            set { _OrignalReportData = value; }
        }

        private FilterProperty _FilterProperty=new FilterProperty();

        public FilterProperty FilterProperty
        {
            get { return _FilterProperty; }
            set { _FilterProperty = value; }
        }
        
        
        private List<Accounts> _vTigerAccounts = new List<Accounts>();

        public List<Accounts> vTigerAccounts
        {
            get { return _vTigerAccounts; }
            set { _vTigerAccounts = value; }
        }

        private List<string> _UserEnterField=new List<string>();

        public List<string> UserEnterField
        {
            get { return _UserEnterField; }
            set { _UserEnterField = value; }
        }

        private List<Modules> _vTigerModules=new List<Modules>();

        public List<Modules> vTigerModules
        {
            get { return _vTigerModules; }
            set { _vTigerModules = value; }
        }
        private VtigerComm.Data.User _User;

        public VtigerComm.Data.User User
        {
            get { return _User; }
            set { _User = value; }
        }


        private List<Comparision> _ComparisionCollection=new List<Comparision>();

        public List<Comparision> ComparisionCollection
        {
            get { return _ComparisionCollection; }
            set { _ComparisionCollection = value; }
        }


      

        private string _CurrentLanguage=string.Empty;

        public string CurrentLanguage
        {
            get { return _CurrentLanguage; }
            set { _CurrentLanguage = value; }
        }
        
        
        
    }

    public class FilterProperty
    {
        private string _FilterModule;

        public string FilterModule
        {
            get { return _FilterModule; }
            set { _FilterModule = value; }
        }

        private string _FilderField;

        public string FilderField
        {
            get { return _FilderField; }
            set { _FilderField = value; }
        }
        private string _FilterOperation;

        public string FilterOperation
        {
            get { return _FilterOperation; }
            set { _FilterOperation = value; }
        }

        private string _FilterValue;

        public string FilterValue
        {
            get { return _FilterValue; }
            set { _FilterValue = value; }
        }



        private string _FilterModule1;

        public string FilterModule1
        {
            get { return _FilterModule1; }
            set { _FilterModule1 = value; }
        }

        private string _FilderField1;

        public string FilderField1
        {
            get { return _FilderField1; }
            set { _FilderField1 = value; }
        }
        private string _FilterOperation1;

        public string FilterOperation1
        {
            get { return _FilterOperation1; }
            set { _FilterOperation1 = value; }
        }

        private string _FilterValue1;

        public string FilterValue1
        {
            get { return _FilterValue1; }
            set { _FilterValue1 = value; }
        }


        private string _FilterModule2;

        public string FilterModule2
        {
            get { return _FilterModule2; }
            set { _FilterModule2 = value; }
        }

        private string _FilderField2;

        public string FilderField2
        {
            get { return _FilderField2; }
            set { _FilderField2 = value; }
        }
        private string _FilterOperation2;

        public string FilterOperation2
        {
            get { return _FilterOperation2; }
            set { _FilterOperation2 = value; }
        }

        private string _FilterValue2;

        public string FilterValue2
        {
            get { return _FilterValue2; }
            set { _FilterValue2 = value; }
        }


        private string _FilterModule3;

        public string FilterModule3
        {
            get { return _FilterModule3; }
            set { _FilterModule3 = value; }
        }

        private string _FilderField3;

        public string FilderField3
        {
            get { return _FilderField3; }
            set { _FilderField3 = value; }
        }
        private string _FilterOperation3;

        public string FilterOperation3
        {
            get { return _FilterOperation3; }
            set { _FilterOperation3 = value; }
        }

        private string _FilterValue3;

        public string FilterValue3
        {
            get { return _FilterValue3; }
            set { _FilterValue3 = value; }
        }

        private string _FilterModule4;

        public string FilterModule4
        {
            get { return _FilterModule4; }
            set { _FilterModule4 = value; }
        }

        private string _FilderField4;

        public string FilderField4
        {
            get { return _FilderField4; }
            set { _FilderField4 = value; }
        }
        private string _FilterOperation4;

        public string FilterOperation4
        {
            get { return _FilterOperation4; }
            set { _FilterOperation4 = value; }
        }

        private string _FilterValue4;

        public string FilterValue4
        {
            get { return _FilterValue4; }
            set { _FilterValue4 = value; }
        }
    }
}
