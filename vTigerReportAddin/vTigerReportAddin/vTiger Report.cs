﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Tools.Ribbon;
using vTigerReportAddin.Dialog;
using vTigerReportAddin.Models;

namespace vTigerReportAddin
{
    public partial class vTiger_Report
    {
        private void vTiger_Report_Load(object sender, RibbonUIEventArgs e)
        {
            Microsoft.Office.Tools.Ribbon.RibbonDropDownItem dd =
             Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
            dd.Tag = Language.English;
            dd.Label = Language.English.ToString();

            Microsoft.Office.Tools.Ribbon.RibbonDropDownItem de =
             Globals.Factory.GetRibbonFactory().CreateRibbonDropDownItem();
            de.Tag = Language.German;
            de.Label = Language.German.ToString();
            ddllanguage.Items.Add(dd);
            ddllanguage.Items.Add(de);

        }

        private void btnAccessData_Click(object sender, RibbonControlEventArgs e)
        {
            vTigerLogin login = new vTigerLogin();
            login.Show();
        }



        private void btnCreateFieldMapping_Click(object sender, RibbonControlEventArgs e)
        {
            FieldMappingDialog dig = new FieldMappingDialog();
            dig.Show();
        }

        private void btnLoadMapping_Click(object sender, RibbonControlEventArgs e)
        {
            LoadFieldMappingDialog dig = new LoadFieldMappingDialog();
            dig.Show();
        }

        private void btnCreateReport_Click(object sender, RibbonControlEventArgs e)
        {
            ReportGenerationdialog diag = new ReportGenerationdialog();
            diag.Show();
        }

        private void ddllanguage_SelectionChanged(object sender, RibbonControlEventArgs e)
        {
            if (ddllanguage != null && ddllanguage.SelectedItem != null)
            {
                switch (ddllanguage.SelectedItem.ToString())
                {
                    case "German":
                        Globals.ThisAddIn.BussinessData.CurrentLanguage = "de-DE";
                        break;
                    case "English":
                        Globals.ThisAddIn.BussinessData.CurrentLanguage = "en";
                        break;

                    default:
                        break;
                }
            }

            ChangeRibbonCulture();

        }
        private void ChangeRibbonCulture()
        {
            CultureChange chng = new CultureChange("MainRibbon", Globals.ThisAddIn.BussinessData.CurrentLanguage);
            if (string.IsNullOrEmpty(btnAccessData.Label)==false)
            {
                btnAccessData.Label = chng.getValue("btnAccessData");
                grpaccessdata.Label = btnAccessData.Label;
            }
            btnLoadFieldMapping.Label = chng.getValue("createfieldmapping");
            grploadfield.Label = btnLoadFieldMapping.Label;

            btnLoadMapping.Label = chng.getValue("loadfieldmapping");
            grpcreate.Label = btnLoadMapping.Label;
            btnCreateReport.Label = chng.getValue("createreport");
            grpreport.Label = btnCreateReport.Label;
            
            
        }

    }
}
