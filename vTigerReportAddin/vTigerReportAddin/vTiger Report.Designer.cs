﻿namespace vTigerReportAddin
{
    partial class vTiger_Report : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public vTiger_Report()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab1 = this.Factory.CreateRibbonTab();
            this.grpaccessdata = this.Factory.CreateRibbonGroup();
            this.btnAccessData = this.Factory.CreateRibbonButton();
            this.grploadfield = this.Factory.CreateRibbonGroup();
            this.btnLoadFieldMapping = this.Factory.CreateRibbonButton();
            this.grpcreate = this.Factory.CreateRibbonGroup();
            this.btnLoadMapping = this.Factory.CreateRibbonButton();
            this.grpreport = this.Factory.CreateRibbonGroup();
            this.btnCreateReport = this.Factory.CreateRibbonButton();
            this.group5 = this.Factory.CreateRibbonGroup();
            this.ddllanguage = this.Factory.CreateRibbonDropDown();
            this.tab1.SuspendLayout();
            this.grpaccessdata.SuspendLayout();
            this.grploadfield.SuspendLayout();
            this.grpcreate.SuspendLayout();
            this.grpreport.SuspendLayout();
            this.group5.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab1.Groups.Add(this.grpaccessdata);
            this.tab1.Groups.Add(this.grploadfield);
            this.tab1.Groups.Add(this.grpcreate);
            this.tab1.Groups.Add(this.grpreport);
            this.tab1.Groups.Add(this.group5);
            this.tab1.Label = "vTiger Report";
            this.tab1.Name = "tab1";
            // 
            // grpaccessdata
            // 
            this.grpaccessdata.Items.Add(this.btnAccessData);
            this.grpaccessdata.Label = "Access Data";
            this.grpaccessdata.Name = "grpaccessdata";
            // 
            // btnAccessData
            // 
            this.btnAccessData.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnAccessData.Image = global::vTigerReportAddin.Properties.Resources._lock;
            this.btnAccessData.Label = "Access Data";
            this.btnAccessData.Name = "btnAccessData";
            this.btnAccessData.ShowImage = true;
            this.btnAccessData.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnAccessData_Click);
            // 
            // grploadfield
            // 
            this.grploadfield.Items.Add(this.btnLoadFieldMapping);
            this.grploadfield.Label = "Create Field Mapping";
            this.grploadfield.Name = "grploadfield";
            // 
            // btnLoadFieldMapping
            // 
            this.btnLoadFieldMapping.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnLoadFieldMapping.Enabled = false;
            this.btnLoadFieldMapping.Image = global::vTigerReportAddin.Properties.Resources.loadmap;
            this.btnLoadFieldMapping.Label = "Create Field Mapping";
            this.btnLoadFieldMapping.Name = "btnLoadFieldMapping";
            this.btnLoadFieldMapping.ShowImage = true;
            this.btnLoadFieldMapping.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnCreateFieldMapping_Click);
            // 
            // grpcreate
            // 
            this.grpcreate.Items.Add(this.btnLoadMapping);
            this.grpcreate.Label = "Load Field Mapping";
            this.grpcreate.Name = "grpcreate";
            // 
            // btnLoadMapping
            // 
            this.btnLoadMapping.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnLoadMapping.Enabled = false;
            this.btnLoadMapping.Image = global::vTigerReportAddin.Properties.Resources.loadmap;
            this.btnLoadMapping.Label = "Load Field Mapping";
            this.btnLoadMapping.Name = "btnLoadMapping";
            this.btnLoadMapping.ShowImage = true;
            this.btnLoadMapping.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnLoadMapping_Click);
            // 
            // grpreport
            // 
            this.grpreport.Items.Add(this.btnCreateReport);
            this.grpreport.Label = "Create Report";
            this.grpreport.Name = "grpreport";
            // 
            // btnCreateReport
            // 
            this.btnCreateReport.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
            this.btnCreateReport.Enabled = false;
            this.btnCreateReport.Image = global::vTigerReportAddin.Properties.Resources.Reports;
            this.btnCreateReport.Label = "Create Report";
            this.btnCreateReport.Name = "btnCreateReport";
            this.btnCreateReport.ShowImage = true;
            this.btnCreateReport.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.btnCreateReport_Click);
            // 
            // group5
            // 
            this.group5.Items.Add(this.ddllanguage);
            this.group5.Label = "Language";
            this.group5.Name = "group5";
            // 
            // ddllanguage
            // 
            this.ddllanguage.Label = "Select Language";
            this.ddllanguage.Name = "ddllanguage";
            this.ddllanguage.SelectionChanged += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.ddllanguage_SelectionChanged);
            // 
            // vTiger_Report
            // 
            this.Name = "vTiger_Report";
            this.RibbonType = "Microsoft.Word.Document";
            this.Tabs.Add(this.tab1);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.vTiger_Report_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.grpaccessdata.ResumeLayout(false);
            this.grpaccessdata.PerformLayout();
            this.grploadfield.ResumeLayout(false);
            this.grploadfield.PerformLayout();
            this.grpcreate.ResumeLayout(false);
            this.grpcreate.PerformLayout();
            this.grpreport.ResumeLayout(false);
            this.grpreport.PerformLayout();
            this.group5.ResumeLayout(false);
            this.group5.PerformLayout();

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grpaccessdata;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnAccessData;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grploadfield;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnLoadFieldMapping;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grpcreate;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnLoadMapping;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup grpreport;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton btnCreateReport;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group5;
        internal Microsoft.Office.Tools.Ribbon.RibbonDropDown ddllanguage;
    }

    partial class ThisRibbonCollection
    {
        internal vTiger_Report vTiger_Report
        {
            get { return this.GetRibbon<vTiger_Report>(); }
        }
    }
}
