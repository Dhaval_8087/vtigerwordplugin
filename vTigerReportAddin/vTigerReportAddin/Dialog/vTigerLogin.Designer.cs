﻿namespace vTigerReportAddin.Dialog
{
    partial class vTigerLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblusername = new System.Windows.Forms.Label();
            this.txtUsername = new System.Windows.Forms.TextBox();
            this.btnok = new System.Windows.Forms.Button();
            this.lblaccesskey = new System.Windows.Forms.Label();
            this.txtaccesskey = new System.Windows.Forms.TextBox();
            this.lblwebserviceurl = new System.Windows.Forms.Label();
            this.txtwebserviceurl = new System.Windows.Forms.TextBox();
            this.btncancel = new System.Windows.Forms.Button();
            this.headertext = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.CreateContact = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblusername
            // 
            this.lblusername.AutoSize = true;
            this.lblusername.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblusername.Location = new System.Drawing.Point(39, 63);
            this.lblusername.Name = "lblusername";
            this.lblusername.Size = new System.Drawing.Size(87, 20);
            this.lblusername.TabIndex = 0;
            this.lblusername.Text = "Username:";
            // 
            // txtUsername
            // 
            this.txtUsername.Location = new System.Drawing.Point(42, 89);
            this.txtUsername.Multiline = true;
            this.txtUsername.Name = "txtUsername";
            this.txtUsername.Size = new System.Drawing.Size(313, 25);
            this.txtUsername.TabIndex = 1;
            this.txtUsername.Leave += new System.EventHandler(this.txtUsername_Leave);
            // 
            // btnok
            // 
            this.btnok.Enabled = false;
            this.btnok.Location = new System.Drawing.Point(106, 302);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(75, 31);
            this.btnok.TabIndex = 2;
            this.btnok.Text = "OK";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // lblaccesskey
            // 
            this.lblaccesskey.AutoSize = true;
            this.lblaccesskey.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblaccesskey.Location = new System.Drawing.Point(39, 133);
            this.lblaccesskey.Name = "lblaccesskey";
            this.lblaccesskey.Size = new System.Drawing.Size(95, 20);
            this.lblaccesskey.TabIndex = 3;
            this.lblaccesskey.Text = "Access Key:";
            // 
            // txtaccesskey
            // 
            this.txtaccesskey.Location = new System.Drawing.Point(42, 160);
            this.txtaccesskey.Multiline = true;
            this.txtaccesskey.Name = "txtaccesskey";
            this.txtaccesskey.Size = new System.Drawing.Size(313, 24);
            this.txtaccesskey.TabIndex = 4;
            this.txtaccesskey.Leave += new System.EventHandler(this.txtaccesskey_Leave);
            // 
            // lblwebserviceurl
            // 
            this.lblwebserviceurl.AutoSize = true;
            this.lblwebserviceurl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblwebserviceurl.Location = new System.Drawing.Point(39, 211);
            this.lblwebserviceurl.Name = "lblwebserviceurl";
            this.lblwebserviceurl.Size = new System.Drawing.Size(132, 20);
            this.lblwebserviceurl.TabIndex = 5;
            this.lblwebserviceurl.Text = "Webservice URL:";
            // 
            // txtwebserviceurl
            // 
            this.txtwebserviceurl.Location = new System.Drawing.Point(42, 241);
            this.txtwebserviceurl.Multiline = true;
            this.txtwebserviceurl.Name = "txtwebserviceurl";
            this.txtwebserviceurl.Size = new System.Drawing.Size(313, 26);
            this.txtwebserviceurl.TabIndex = 6;
            this.txtwebserviceurl.Leave += new System.EventHandler(this.txtwebserviceurl_Leave);
            // 
            // btncancel
            // 
            this.btncancel.Location = new System.Drawing.Point(203, 302);
            this.btncancel.Name = "btncancel";
            this.btncancel.Size = new System.Drawing.Size(75, 31);
            this.btncancel.TabIndex = 7;
            this.btncancel.Text = "Cancel";
            this.btncancel.UseVisualStyleBackColor = true;
            this.btncancel.Click += new System.EventHandler(this.btncancel_Click);
            // 
            // headertext
            // 
            this.headertext.AutoSize = true;
            this.headertext.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.headertext.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.headertext.Location = new System.Drawing.Point(140, 4);
            this.headertext.Name = "headertext";
            this.headertext.Size = new System.Drawing.Size(115, 24);
            this.headertext.TabIndex = 8;
            this.headertext.Text = "vTiger Login";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(418, 351);
            this.shapeContainer1.TabIndex = 9;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderWidth = 2;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 1;
            this.lineShape1.X2 = 416;
            this.lineShape1.Y1 = 36;
            this.lineShape1.Y2 = 36;
            // 
            // CreateContact
            // 
            this.CreateContact.Location = new System.Drawing.Point(299, 302);
            this.CreateContact.Name = "CreateContact";
            this.CreateContact.Size = new System.Drawing.Size(116, 31);
            this.CreateContact.TabIndex = 10;
            this.CreateContact.Text = "CreateContactentity";
            this.CreateContact.UseVisualStyleBackColor = true;
            this.CreateContact.Visible = false;
            this.CreateContact.Click += new System.EventHandler(this.CreateContact_Click);
            // 
            // vTigerLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(418, 351);
            this.Controls.Add(this.CreateContact);
            this.Controls.Add(this.headertext);
            this.Controls.Add(this.btncancel);
            this.Controls.Add(this.txtwebserviceurl);
            this.Controls.Add(this.lblwebserviceurl);
            this.Controls.Add(this.txtaccesskey);
            this.Controls.Add(this.lblaccesskey);
            this.Controls.Add(this.btnok);
            this.Controls.Add(this.txtUsername);
            this.Controls.Add(this.lblusername);
            this.Controls.Add(this.shapeContainer1);
            this.Name = "vTigerLogin";
            this.Text = "vTigerLogin";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblusername;
        private System.Windows.Forms.TextBox txtUsername;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.Label lblaccesskey;
        private System.Windows.Forms.TextBox txtaccesskey;
        private System.Windows.Forms.Label lblwebserviceurl;
        private System.Windows.Forms.TextBox txtwebserviceurl;
        private System.Windows.Forms.Button btncancel;
        private System.Windows.Forms.Label headertext;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.Button CreateContact;
    }
}