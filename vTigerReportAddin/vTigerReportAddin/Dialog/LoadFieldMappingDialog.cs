﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using vTigerReportAddin.Models;

namespace vTigerReportAddin.Dialog
{
    public partial class LoadFieldMappingDialog : Form
    {
       
        public LoadFieldMappingDialog()
        {
            InitializeComponent();
            MaximizeBox = false;
            MinimizeBox = false;
            vTigerLoadgrid.AllowUserToAddRows = false;
            SetCulture(); 
        }

        private void SetCulture()
        {
            CultureChange chng = new CultureChange("LoadFieldMappingDialog", Globals.ThisAddIn.BussinessData.CurrentLanguage);
            this.Text = chng.getValue("Title");
            lblheadertext.Text = chng.getValue("HeaderText");
            lblselectfolderpath.Text = chng.getValue("folderpath");
            lblselectmapping.Text = chng.getValue("selectmapping");
            go.Text = chng.getValue("go");
            Browse.Text = chng.getValue("browse");
            btnUpdate.Text = chng.getValue("update");
            
        }
        private void Browse_Click(object sender, EventArgs e)
        {    
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            DialogResult result = dialog.ShowDialog();
            txtPath.Text = dialog.SelectedPath;
            // OK button was pressed. 
            if (result == DialogResult.OK)
            {
                var path = dialog.SelectedPath;
                var files = Directory.EnumerateFiles(path, "*.xml");
                List<LoadFiles> Lfiles = new List<LoadFiles>();
                foreach (var item in files)
                {
                    LoadFiles lf = new LoadFiles();
                    lf.FileName = Path.GetFileName(item);
                    lf.FilePath = item;
                    Lfiles.Add(lf);
                }

                LoadMapped.DataSource = Lfiles;
                LoadMapped.DisplayMember = "FileName";
                LoadMapped.ValueMember = "FilePath";
            }
        }

        private void go_Click(object sender, EventArgs e)
        {
            try
            {
                var path = LoadMapped.SelectedValue;
                DataTable dt = new DataTable();

                dt.Columns.Add(new DataColumn("Mapping", typeof(bool)));
                dt.Columns.Add("Merge Field");
                dt.Columns.Add("Module");
                dt.Columns.Add("vTiger Field");
                DataSet ds = new DataSet();
                ds.ReadXml(path.ToString());


                foreach (DataRow inner in ds.Tables[1].Rows)
                {
                    DataRow row = dt.NewRow();
                    row["Mapping"] = true;
                    row["Merge Field"] = inner["vMergeField"].ToString();
                    row["Module"] = inner["vModule"].ToString();
                    row["vTiger Field"] = inner["vTigerField"].ToString();
                    dt.Rows.Add(row);

                }
                vTigerLoadgrid.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Wrong File   "+ex.Message);
            }
            
            

        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            var FilePath = LoadMapped.SelectedValue.ToString();

        }
    }
    public class LoadFiles
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }
}
