﻿namespace vTigerReportAddin.Dialog
{
    partial class FieldMappingDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblheadertext = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lbltitle = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.lbldescription = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.vtigergrid = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.lblmergefield = new System.Windows.Forms.Label();
            this.mergefieldcombo = new System.Windows.Forms.ComboBox();
            this.lblvtigerield = new System.Windows.Forms.Label();
            this.vtigercombo = new System.Windows.Forms.ComboBox();
            this.lblmodule = new System.Windows.Forms.Label();
            this.vTigerAccount = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.vtigergrid)).BeginInit();
            this.SuspendLayout();
            // 
            // lblheadertext
            // 
            this.lblheadertext.AutoSize = true;
            this.lblheadertext.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblheadertext.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblheadertext.Location = new System.Drawing.Point(229, 9);
            this.lblheadertext.Name = "lblheadertext";
            this.lblheadertext.Size = new System.Drawing.Size(189, 24);
            this.lblheadertext.TabIndex = 0;
            this.lblheadertext.Text = "Field Mapping Dialog";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape2,
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(623, 548);
            this.shapeContainer1.TabIndex = 1;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderWidth = 2;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 0;
            this.lineShape2.X2 = 621;
            this.lineShape2.Y1 = 503;
            this.lineShape2.Y2 = 503;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderWidth = 2;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 1;
            this.lineShape1.X2 = 621;
            this.lineShape1.Y1 = 41;
            this.lineShape1.Y2 = 41;
            // 
            // lbltitle
            // 
            this.lbltitle.AutoSize = true;
            this.lbltitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltitle.Location = new System.Drawing.Point(31, 48);
            this.lbltitle.Name = "lbltitle";
            this.lbltitle.Size = new System.Drawing.Size(42, 20);
            this.lbltitle.TabIndex = 2;
            this.lbltitle.Text = "Title:";
            // 
            // txtTitle
            // 
            this.txtTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTitle.Location = new System.Drawing.Point(35, 72);
            this.txtTitle.Multiline = true;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(561, 30);
            this.txtTitle.TabIndex = 3;
            // 
            // lbldescription
            // 
            this.lbldescription.AutoSize = true;
            this.lbldescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldescription.Location = new System.Drawing.Point(31, 109);
            this.lbldescription.Name = "lbldescription";
            this.lbldescription.Size = new System.Drawing.Size(93, 20);
            this.lbldescription.TabIndex = 4;
            this.lbldescription.Text = "Description:";
            // 
            // txtDescription
            // 
            this.txtDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescription.Location = new System.Drawing.Point(35, 135);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(561, 90);
            this.txtDescription.TabIndex = 5;
            // 
            // vtigergrid
            // 
            this.vtigergrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.vtigergrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.vtigergrid.Location = new System.Drawing.Point(35, 319);
            this.vtigergrid.Name = "vtigergrid";
            this.vtigergrid.Size = new System.Drawing.Size(576, 169);
            this.vtigergrid.TabIndex = 6;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(536, 284);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Add_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(35, 513);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 9;
            this.button3.Text = "Save";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Save_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(126, 513);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 23);
            this.button4.TabIndex = 10;
            this.button4.Text = "Cancel";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // lblmergefield
            // 
            this.lblmergefield.AutoSize = true;
            this.lblmergefield.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmergefield.Location = new System.Drawing.Point(44, 287);
            this.lblmergefield.Name = "lblmergefield";
            this.lblmergefield.Size = new System.Drawing.Size(92, 20);
            this.lblmergefield.TabIndex = 11;
            this.lblmergefield.Text = "Merge Field";
            // 
            // mergefieldcombo
            // 
            this.mergefieldcombo.FormattingEnabled = true;
            this.mergefieldcombo.Location = new System.Drawing.Point(165, 286);
            this.mergefieldcombo.Name = "mergefieldcombo";
            this.mergefieldcombo.Size = new System.Drawing.Size(121, 21);
            this.mergefieldcombo.TabIndex = 12;
            this.mergefieldcombo.TextChanged += new System.EventHandler(this.mergefieldcombo_TextChanged);
            // 
            // lblvtigerield
            // 
            this.lblvtigerield.AutoSize = true;
            this.lblvtigerield.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblvtigerield.Location = new System.Drawing.Point(302, 287);
            this.lblvtigerield.Name = "lblvtigerield";
            this.lblvtigerield.Size = new System.Drawing.Size(89, 20);
            this.lblvtigerield.TabIndex = 13;
            this.lblvtigerield.Text = "vTiger Field";
            // 
            // vtigercombo
            // 
            this.vtigercombo.FormattingEnabled = true;
            this.vtigercombo.Location = new System.Drawing.Point(397, 287);
            this.vtigercombo.Name = "vtigercombo";
            this.vtigercombo.Size = new System.Drawing.Size(121, 21);
            this.vtigercombo.TabIndex = 14;
            // 
            // lblmodule
            // 
            this.lblmodule.AutoSize = true;
            this.lblmodule.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmodule.Location = new System.Drawing.Point(44, 248);
            this.lblmodule.Name = "lblmodule";
            this.lblmodule.Size = new System.Drawing.Size(61, 20);
            this.lblmodule.TabIndex = 15;
            this.lblmodule.Text = "Module";
            // 
            // vTigerAccount
            // 
            this.vTigerAccount.FormattingEnabled = true;
            this.vTigerAccount.Location = new System.Drawing.Point(165, 247);
            this.vTigerAccount.Name = "vTigerAccount";
            this.vTigerAccount.Size = new System.Drawing.Size(121, 21);
            this.vTigerAccount.TabIndex = 16;
            this.vTigerAccount.SelectionChangeCommitted += new System.EventHandler(this.vTigerAccount_SelectionChangeCommitted);
            // 
            // FieldMappingDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(623, 548);
            this.Controls.Add(this.vTigerAccount);
            this.Controls.Add(this.lblmodule);
            this.Controls.Add(this.vtigercombo);
            this.Controls.Add(this.lblvtigerield);
            this.Controls.Add(this.mergefieldcombo);
            this.Controls.Add(this.lblmergefield);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.vtigergrid);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.lbldescription);
            this.Controls.Add(this.txtTitle);
            this.Controls.Add(this.lbltitle);
            this.Controls.Add(this.lblheadertext);
            this.Controls.Add(this.shapeContainer1);
            this.Name = "FieldMappingDialog";
            this.Text = "FieldMappingDialog";
            ((System.ComponentModel.ISupportInitialize)(this.vtigergrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblheadertext;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.Label lbltitle;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.Label lbldescription;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.DataGridView vtigergrid;
        private System.Windows.Forms.Button button1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label lblmergefield;
        private System.Windows.Forms.ComboBox mergefieldcombo;
        private System.Windows.Forms.Label lblvtigerield;
        private System.Windows.Forms.ComboBox vtigercombo;
        private System.Windows.Forms.Label lblmodule;
        private System.Windows.Forms.ComboBox vTigerAccount;
    }
}