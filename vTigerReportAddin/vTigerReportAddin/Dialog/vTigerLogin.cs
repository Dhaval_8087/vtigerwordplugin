﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using VtigerComm;
using VtigerComm.Data;
using vTigerReportAddin.CultureResource;
using vTigerReportAddin.Models;

namespace vTigerReportAddin.Dialog
{
    public partial class vTigerLogin : Form
    {
        public string ProgressbarTitle { get; set; }
        public vTigerLogin()
        {
            InitializeComponent();
            MaximizeBox = false;
            MinimizeBox = false;
            this.Load += vTigerLogin_Load;
          //  CultureChange chg = new CultureChange("I18NLogin", "");
            //label1.Text = chg.getValue("Username");

        }

       
        

        void vTigerLogin_Load(object sender, EventArgs e)
        {
            txtUsername.Text = "admin";
            txtaccesskey.Text = "LIVpT39CBpTRe9Xd";
            txtwebserviceurl.Text = @"https://vserv5.etis-gmbh.eu/word/webservice.php";
            IsOkEnabled();
            SetCulture();
            // var abc = Globals.ThisAddIn.Application.WordBasic.Fields;
            //List<string> merge = new List<string>();

        }
        private void SetCulture()
        {
            CultureChange chang = new CultureChange("I18NLogin",Globals.ThisAddIn.BussinessData.CurrentLanguage);
            headertext.Text = chang.getValue("HeaderText");
            lblusername.Text = chang.getValue("Username");
            lblaccesskey.Text = chang.getValue("Accesskey");
            lblwebserviceurl.Text = chang.getValue("Webserviceurl");
            btnok.Text = chang.getValue("OK");
            btncancel.Text = chang.getValue("Cancel");
            this.Text = chang.getValue("Title");
            ProgressbarTitle = chang.getValue("ProgressbarTitle");
        }

        public static List<string> GetMergeFields(List<string> allFields)
        {
            var merges = new List<string>();

            foreach (var field in allFields)
            {
                var isNestedField = false;
                foreach (var fieldChar in field)
                {
                    int charCode = fieldChar;
                    if (charCode == 19 || charCode == 21)
                    {
                        isNestedField = true;
                        break;
                    }
                }
                if (!isNestedField)
                {
                    var fieldCode = field;
                    if (fieldCode.Contains("MERGEFIELD"))
                    {
                        var fieldName = fieldCode.Replace("MERGEFIELD", string.Empty).Replace('"', ' ').Trim();
                        var charsToGet = fieldName.IndexOf(" ");
                        if (charsToGet < 0)
                            charsToGet = fieldName.IndexOf(@"\");

                        charsToGet = charsToGet > 0 ? charsToGet : fieldName.Length;

                        fieldName = fieldName.Substring(0, charsToGet);

                        if (!merges.Contains(fieldName))
                        {
                            merges.Add(fieldName);
                        }
                    }
                }
            }
            return merges;
        }


        private void btnok_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtUsername.Text.Trim()) == false && string.IsNullOrEmpty(txtaccesskey.Text.Trim()) == false
                && string.IsNullOrEmpty(txtwebserviceurl.Text.Trim()) == false)
            {
                try
                {

                    // Initialize the dialog that will contain the progress bar
                    ProgressDialog progressDialog = new ProgressDialog();
                    progressDialog.Text = ProgressbarTitle;
                    // Initialize the thread that will handle the background process
                    Thread backgroundThread = new Thread(
                        new ThreadStart(() =>
                        {
                            // Set the flag that indicates if a process is currently running
                            try
                            {
                                progressDialog.SetIndeterminate(true);
                                VtigerComm.Data.User usr = VtigerLogin.CreateUserToken(txtUsername.Text.Trim(), txtaccesskey.Text.Trim(), txtwebserviceurl.Text.Trim());
                                VTEntity vte = InfoService.getAllFieldsFromEntity(usr, "Accounts");

                                FillData(vte, usr);
                                // Close the dialog if it hasn't been already
                                if (progressDialog.InvokeRequired)
                                    progressDialog.BeginInvoke(new Action(() => progressDialog.Close()));

                            }
                            catch (Exception exp)
                            {
                                MessageBox.Show(exp.Message);
                            }
                           

                        }
                    ));

                    // Start the background process thread
                    backgroundThread.Start();

                    // Open the dialog
                    progressDialog.ShowDialog();


                    // List<VTEntity> lst = VtigerComm.VtigerCRUD.QueryEntities("Select * from Contacts", usr);
                    // var aaa = InfoService.getAllFieldsFromEntity(usr, "Contacts");
                    //List<VTEntity> lst = VtigerComm.VtigerCRUD.QueryEntities("Select * from Contacts", usr);




                }
                catch (Exception ex)
                {
                    MessageBox.Show("Opps...." + ex.Message);
                }

            }

        }
        private void FillData(VTEntity vte, User usr)
        {
            Invoke(new Action(() =>
            {
                Globals.ThisAddIn.BussinessData.User = usr;
                foreach (KeyValuePair<string, Field> elem in vte.Fields)
                {

                    Accounts act = new Accounts();
                    act.AccountName = elem.Key;
                    act.AccountValue = elem.Value.Value;
                    Globals.ThisAddIn.BussinessData.vTigerAccounts.Add(act);

                }
                
                var vtigerModules = InfoService.getAllTypes(usr);
                foreach (var item in vtigerModules)
                {
                    Modules m = new Modules();
                    m.Name = item.ToString();
                    Globals.ThisAddIn.BussinessData.vTigerModules.Add(m);
                }
                Globals.Ribbons.vTiger_Report.btnLoadMapping.Enabled = true;
                Globals.Ribbons.vTiger_Report.btnCreateReport.Enabled = true;
                Globals.Ribbons.vTiger_Report.btnLoadFieldMapping.Enabled = true;
                this.Close();

            }));
        }
        private void btncancel_Click(object sender, EventArgs e)
        {
            txtUsername.Text = string.Empty;
            txtaccesskey.Text = string.Empty;
            txtwebserviceurl.Text = string.Empty;
            btnok.Enabled = false;
           // this.Close();
        }

        private void CreateContact_Click(object sender, EventArgs e)
        {
            VTEntity vtef = InfoService.getAllFieldsFromEntity(Globals.ThisAddIn.BussinessData.User, "Contacts");
            VTEntity vte = new VTEntity();
            try
            {
                foreach (KeyValuePair<string, Field> elem in vtef.Fields)
                {
                    if (elem.Key != "id")
                        vte.setValue(elem.Key, "Dhaval");

                }
                vte.setValue("assigned_user_id", Globals.ThisAddIn.BussinessData.User.userId);
                VtigerCRUD.CreateEntities(Globals.ThisAddIn.BussinessData.User, vte, "Contacts");
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        private void txtUsername_Leave(object sender, EventArgs e)
        {
            IsOkEnabled();
        }

        private void txtaccesskey_Leave(object sender, EventArgs e)
        {
            IsOkEnabled();
        }

        private void txtwebserviceurl_Leave(object sender, EventArgs e)
        {
            IsOkEnabled();
        }

        private void IsOkEnabled()
        {
            if (string.IsNullOrEmpty(txtUsername.Text.Trim()) == false && string.IsNullOrEmpty(txtaccesskey.Text.Trim()) == false
              && string.IsNullOrEmpty(txtwebserviceurl.Text.Trim()) == false)
            {
                btnok.Enabled = true;
            }
            else
            {
                btnok.Enabled = false;
            }
        }
    }
}
