﻿namespace vTigerReportAddin.Dialog
{
    partial class FilterDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ComboModule = new System.Windows.Forms.ComboBox();
            this.ComboField = new System.Windows.Forms.ComboBox();
            this.ComboOpration = new System.Windows.Forms.ComboBox();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.btnok = new System.Windows.Forms.Button();
            this.ComboModule1 = new System.Windows.Forms.ComboBox();
            this.ComboField1 = new System.Windows.Forms.ComboBox();
            this.ComboModule2 = new System.Windows.Forms.ComboBox();
            this.ComboModule3 = new System.Windows.Forms.ComboBox();
            this.ComboModule4 = new System.Windows.Forms.ComboBox();
            this.ComboField2 = new System.Windows.Forms.ComboBox();
            this.ComboField3 = new System.Windows.Forms.ComboBox();
            this.ComboField4 = new System.Windows.Forms.ComboBox();
            this.ComboOpration1 = new System.Windows.Forms.ComboBox();
            this.ComboOpration2 = new System.Windows.Forms.ComboBox();
            this.ComboOpration3 = new System.Windows.Forms.ComboBox();
            this.ComboOpration4 = new System.Windows.Forms.ComboBox();
            this.txtSearch1 = new System.Windows.Forms.TextBox();
            this.txtSearch2 = new System.Windows.Forms.TextBox();
            this.txtSearch3 = new System.Windows.Forms.TextBox();
            this.txtSearch4 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.label1.Location = new System.Drawing.Point(246, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Filter Dialog";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(629, 333);
            this.shapeContainer1.TabIndex = 1;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderWidth = 2;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = 0;
            this.lineShape1.X2 = 621;
            this.lineShape1.Y1 = 50;
            this.lineShape1.Y2 = 50;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(30, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Module";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(197, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Field";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(350, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Operation";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(494, 73);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "Input";
            // 
            // ComboModule
            // 
            this.ComboModule.FormattingEnabled = true;
            this.ComboModule.Location = new System.Drawing.Point(33, 108);
            this.ComboModule.Name = "ComboModule";
            this.ComboModule.Size = new System.Drawing.Size(121, 21);
            this.ComboModule.TabIndex = 6;
            this.ComboModule.SelectionChangeCommitted += new System.EventHandler(this.ComboModule_SelectionChangeCommitted);
            // 
            // ComboField
            // 
            this.ComboField.FormattingEnabled = true;
            this.ComboField.Location = new System.Drawing.Point(200, 108);
            this.ComboField.Name = "ComboField";
            this.ComboField.Size = new System.Drawing.Size(121, 21);
            this.ComboField.TabIndex = 7;
            // 
            // ComboOpration
            // 
            this.ComboOpration.FormattingEnabled = true;
            this.ComboOpration.Location = new System.Drawing.Point(353, 108);
            this.ComboOpration.Name = "ComboOpration";
            this.ComboOpration.Size = new System.Drawing.Size(121, 21);
            this.ComboOpration.TabIndex = 8;
            // 
            // txtSearch
            // 
            this.txtSearch.Location = new System.Drawing.Point(497, 108);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(122, 20);
            this.txtSearch.TabIndex = 9;
            // 
            // btnok
            // 
            this.btnok.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnok.Location = new System.Drawing.Point(544, 281);
            this.btnok.Name = "btnok";
            this.btnok.Size = new System.Drawing.Size(75, 29);
            this.btnok.TabIndex = 10;
            this.btnok.Text = "OK";
            this.btnok.UseVisualStyleBackColor = true;
            this.btnok.Click += new System.EventHandler(this.btnok_Click);
            // 
            // ComboModule1
            // 
            this.ComboModule1.FormattingEnabled = true;
            this.ComboModule1.Location = new System.Drawing.Point(33, 135);
            this.ComboModule1.Name = "ComboModule1";
            this.ComboModule1.Size = new System.Drawing.Size(121, 21);
            this.ComboModule1.TabIndex = 11;
            this.ComboModule1.SelectionChangeCommitted += new System.EventHandler(this.ComboModule_SelectionChangeCommitted);
            // 
            // ComboField1
            // 
            this.ComboField1.FormattingEnabled = true;
            this.ComboField1.Location = new System.Drawing.Point(200, 135);
            this.ComboField1.Name = "ComboField1";
            this.ComboField1.Size = new System.Drawing.Size(121, 21);
            this.ComboField1.TabIndex = 12;
            // 
            // ComboModule2
            // 
            this.ComboModule2.FormattingEnabled = true;
            this.ComboModule2.Location = new System.Drawing.Point(33, 162);
            this.ComboModule2.Name = "ComboModule2";
            this.ComboModule2.Size = new System.Drawing.Size(121, 21);
            this.ComboModule2.TabIndex = 13;
            this.ComboModule2.SelectionChangeCommitted += new System.EventHandler(this.ComboModule_SelectionChangeCommitted);
            // 
            // ComboModule3
            // 
            this.ComboModule3.FormattingEnabled = true;
            this.ComboModule3.Location = new System.Drawing.Point(33, 189);
            this.ComboModule3.Name = "ComboModule3";
            this.ComboModule3.Size = new System.Drawing.Size(121, 21);
            this.ComboModule3.TabIndex = 14;
            this.ComboModule3.SelectionChangeCommitted += new System.EventHandler(this.ComboModule_SelectionChangeCommitted);
            // 
            // ComboModule4
            // 
            this.ComboModule4.FormattingEnabled = true;
            this.ComboModule4.Location = new System.Drawing.Point(33, 216);
            this.ComboModule4.Name = "ComboModule4";
            this.ComboModule4.Size = new System.Drawing.Size(121, 21);
            this.ComboModule4.TabIndex = 15;
            this.ComboModule4.SelectionChangeCommitted += new System.EventHandler(this.ComboModule_SelectionChangeCommitted);
            // 
            // ComboField2
            // 
            this.ComboField2.FormattingEnabled = true;
            this.ComboField2.Location = new System.Drawing.Point(200, 162);
            this.ComboField2.Name = "ComboField2";
            this.ComboField2.Size = new System.Drawing.Size(121, 21);
            this.ComboField2.TabIndex = 16;
            // 
            // ComboField3
            // 
            this.ComboField3.FormattingEnabled = true;
            this.ComboField3.Location = new System.Drawing.Point(200, 189);
            this.ComboField3.Name = "ComboField3";
            this.ComboField3.Size = new System.Drawing.Size(121, 21);
            this.ComboField3.TabIndex = 17;
            // 
            // ComboField4
            // 
            this.ComboField4.FormattingEnabled = true;
            this.ComboField4.Location = new System.Drawing.Point(200, 216);
            this.ComboField4.Name = "ComboField4";
            this.ComboField4.Size = new System.Drawing.Size(121, 21);
            this.ComboField4.TabIndex = 18;
            // 
            // ComboOpration1
            // 
            this.ComboOpration1.FormattingEnabled = true;
            this.ComboOpration1.Location = new System.Drawing.Point(353, 135);
            this.ComboOpration1.Name = "ComboOpration1";
            this.ComboOpration1.Size = new System.Drawing.Size(121, 21);
            this.ComboOpration1.TabIndex = 19;
            // 
            // ComboOpration2
            // 
            this.ComboOpration2.FormattingEnabled = true;
            this.ComboOpration2.Location = new System.Drawing.Point(353, 162);
            this.ComboOpration2.Name = "ComboOpration2";
            this.ComboOpration2.Size = new System.Drawing.Size(121, 21);
            this.ComboOpration2.TabIndex = 20;
            // 
            // ComboOpration3
            // 
            this.ComboOpration3.FormattingEnabled = true;
            this.ComboOpration3.Location = new System.Drawing.Point(353, 189);
            this.ComboOpration3.Name = "ComboOpration3";
            this.ComboOpration3.Size = new System.Drawing.Size(121, 21);
            this.ComboOpration3.TabIndex = 21;
            // 
            // ComboOpration4
            // 
            this.ComboOpration4.FormattingEnabled = true;
            this.ComboOpration4.Location = new System.Drawing.Point(353, 216);
            this.ComboOpration4.Name = "ComboOpration4";
            this.ComboOpration4.Size = new System.Drawing.Size(121, 21);
            this.ComboOpration4.TabIndex = 22;
            // 
            // txtSearch1
            // 
            this.txtSearch1.Location = new System.Drawing.Point(497, 134);
            this.txtSearch1.Name = "txtSearch1";
            this.txtSearch1.Size = new System.Drawing.Size(122, 20);
            this.txtSearch1.TabIndex = 23;
            // 
            // txtSearch2
            // 
            this.txtSearch2.Location = new System.Drawing.Point(497, 163);
            this.txtSearch2.Name = "txtSearch2";
            this.txtSearch2.Size = new System.Drawing.Size(122, 20);
            this.txtSearch2.TabIndex = 24;
            // 
            // txtSearch3
            // 
            this.txtSearch3.Location = new System.Drawing.Point(497, 190);
            this.txtSearch3.Name = "txtSearch3";
            this.txtSearch3.Size = new System.Drawing.Size(122, 20);
            this.txtSearch3.TabIndex = 25;
            // 
            // txtSearch4
            // 
            this.txtSearch4.Location = new System.Drawing.Point(497, 217);
            this.txtSearch4.Name = "txtSearch4";
            this.txtSearch4.Size = new System.Drawing.Size(122, 20);
            this.txtSearch4.TabIndex = 26;
            // 
            // FilterDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(629, 333);
            this.Controls.Add(this.txtSearch4);
            this.Controls.Add(this.txtSearch3);
            this.Controls.Add(this.txtSearch2);
            this.Controls.Add(this.txtSearch1);
            this.Controls.Add(this.ComboOpration4);
            this.Controls.Add(this.ComboOpration3);
            this.Controls.Add(this.ComboOpration2);
            this.Controls.Add(this.ComboOpration1);
            this.Controls.Add(this.ComboField4);
            this.Controls.Add(this.ComboField3);
            this.Controls.Add(this.ComboField2);
            this.Controls.Add(this.ComboModule4);
            this.Controls.Add(this.ComboModule3);
            this.Controls.Add(this.ComboModule2);
            this.Controls.Add(this.ComboField1);
            this.Controls.Add(this.ComboModule1);
            this.Controls.Add(this.btnok);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.ComboOpration);
            this.Controls.Add(this.ComboField);
            this.Controls.Add(this.ComboModule);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.shapeContainer1);
            this.Name = "FilterDialog";
            this.Text = "FilterDialog";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox ComboModule;
        private System.Windows.Forms.ComboBox ComboField;
        private System.Windows.Forms.ComboBox ComboOpration;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Button btnok;
        private System.Windows.Forms.ComboBox ComboModule1;
        private System.Windows.Forms.ComboBox ComboField1;
        private System.Windows.Forms.ComboBox ComboModule2;
        private System.Windows.Forms.ComboBox ComboModule3;
        private System.Windows.Forms.ComboBox ComboModule4;
        private System.Windows.Forms.ComboBox ComboField2;
        private System.Windows.Forms.ComboBox ComboField3;
        private System.Windows.Forms.ComboBox ComboField4;
        private System.Windows.Forms.ComboBox ComboOpration1;
        private System.Windows.Forms.ComboBox ComboOpration2;
        private System.Windows.Forms.ComboBox ComboOpration3;
        private System.Windows.Forms.ComboBox ComboOpration4;
        private System.Windows.Forms.TextBox txtSearch1;
        private System.Windows.Forms.TextBox txtSearch2;
        private System.Windows.Forms.TextBox txtSearch3;
        private System.Windows.Forms.TextBox txtSearch4;
    }
}