﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using VtigerComm.Data;

namespace vTigerReportAddin.Dialog
{
    public partial class FilterDialog : Form
    {
        public delegate void FilterDialogHandler();
        public event FilterDialogHandler FilterReturn;

        public FilterDialog()
        {
            InitializeComponent();
            MaximizeBox = false;
            MinimizeBox = false;
            ComboModule.DataSource = Globals.ThisAddIn.BussinessData.vTigerModules.Select(p => p.Name).ToList();
            ComboModule1.DataSource = Globals.ThisAddIn.BussinessData.vTigerModules.Select(p => p.Name).ToList();
            ComboModule2.DataSource = Globals.ThisAddIn.BussinessData.vTigerModules.Select(p => p.Name).ToList();
            ComboModule3.DataSource = Globals.ThisAddIn.BussinessData.vTigerModules.Select(p => p.Name).ToList();
            ComboModule4.DataSource = Globals.ThisAddIn.BussinessData.vTigerModules.Select(p => p.Name).ToList();

            ComboOpration.DataSource = Globals.ThisAddIn.BussinessData.ComparisionCollection.Select(p => p.Name).ToList();
            ComboOpration1.DataSource = Globals.ThisAddIn.BussinessData.ComparisionCollection.Select(p => p.Name).ToList();
            ComboOpration2.DataSource = Globals.ThisAddIn.BussinessData.ComparisionCollection.Select(p => p.Name).ToList();
            ComboOpration3.DataSource = Globals.ThisAddIn.BussinessData.ComparisionCollection.Select(p => p.Name).ToList();
            ComboOpration4.DataSource = Globals.ThisAddIn.BussinessData.ComparisionCollection.Select(p => p.Name).ToList();


            this.Load += (ea, sa) =>
            {
                ComboModule.SelectedIndex = 0;
                ComboModule_SelectionChangeCommitted(ComboModule, sa);

                ComboModule1.SelectedIndex = 0;
                ComboModule_SelectionChangeCommitted(ComboModule1, sa);

                ComboModule2.SelectedIndex = 0;
                ComboModule_SelectionChangeCommitted(ComboModule2, sa);


                ComboModule3.SelectedIndex = 0;
                ComboModule_SelectionChangeCommitted(ComboModule3, sa);


                ComboModule4.SelectedIndex = 0;
                ComboModule_SelectionChangeCommitted(ComboModule4, sa);



            };
        }

        private void ComboModule_SelectionChangeCommitted(object sender, EventArgs e)
        {
            try
            {
                // Initialize the dialog that will contain the progress bar
                ProgressDialog progressDialog = new ProgressDialog();
                ComboBox Module = sender as ComboBox;

                string Value = Module.SelectedValue.ToString();
                // Initialize the thread that will handle the background process
                Thread backgroundThread = new Thread(
                    new ThreadStart(() =>
                    {
                        // Set the flag that indicates if a process is currently running

                        progressDialog.SetIndeterminate(true);
                        VTEntity vte = InfoService.getAllFieldsFromEntity(Globals.ThisAddIn.BussinessData.User, Value);
                        if (Module.Name == "ComboModule")
                            fillCombo(vte, ComboField);
                        else if (Module.Name == "ComboModule1")
                            fillCombo(vte, ComboField1);
                        else if (Module.Name == "ComboModule2")
                            fillCombo(vte, ComboField2);
                        else if (Module.Name == "ComboModule3")
                            fillCombo(vte, ComboField3);
                        else if (Module.Name == "ComboModule4")
                            fillCombo(vte, ComboField4);


                        // Close the dialog if it hasn't been already
                        if (progressDialog.InvokeRequired)
                            progressDialog.BeginInvoke(new Action(() => progressDialog.Close()));


                    }
                ));

                // Start the background process thread
                backgroundThread.Start();

                // Open the dialog
                progressDialog.ShowDialog();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Oppss!!!! " + ex.Message);
            }
        }

        private void fillCombo(VTEntity vte, ComboBox cmb)
        {
            List<string> Entity = new List<string>();
            foreach (KeyValuePair<string, Field> elem in vte.Fields)
            {
                Entity.Add(elem.Key.ToString());
            }
            Invoke(new Action(() => { cmb.DataSource = Entity; }));


        }

        private void btnok_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ComboModule.SelectedValue.ToString()) == false)
            {
                try
                {
                    
                    Globals.ThisAddIn.BussinessData.FilterProperty.FilterModule = ComboModule.SelectedValue.ToString();
                    Globals.ThisAddIn.BussinessData.FilterProperty.FilderField = ComboField.SelectedValue.ToString();
                    Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation = ComboOpration.SelectedValue.ToString();
                    Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue = txtSearch.Text;

                    Globals.ThisAddIn.BussinessData.FilterProperty.FilterModule1 = ComboModule1.SelectedValue.ToString();
                    Globals.ThisAddIn.BussinessData.FilterProperty.FilderField1 = ComboField1.SelectedValue.ToString();
                    Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation1 = ComboOpration1.SelectedValue.ToString();
                    Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue1 = txtSearch1.Text;


                    Globals.ThisAddIn.BussinessData.FilterProperty.FilterModule2 = ComboModule2.SelectedValue.ToString();
                    Globals.ThisAddIn.BussinessData.FilterProperty.FilderField2 = ComboField2.SelectedValue.ToString();
                    Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation2 = ComboOpration2.SelectedValue.ToString();
                    Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue2 = txtSearch2.Text;

                    Globals.ThisAddIn.BussinessData.FilterProperty.FilterModule3 = ComboModule3.SelectedValue.ToString();
                    Globals.ThisAddIn.BussinessData.FilterProperty.FilderField3 = ComboField3.SelectedValue.ToString();
                    Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation3 = ComboOpration3.SelectedValue.ToString();
                    Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue3 = txtSearch3.Text;

                    Globals.ThisAddIn.BussinessData.FilterProperty.FilterModule4 = ComboModule4.SelectedValue.ToString();
                    Globals.ThisAddIn.BussinessData.FilterProperty.FilderField4 = ComboField4.SelectedValue.ToString();
                    Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation4 = ComboOpration4.SelectedValue.ToString();
                    Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue4 = txtSearch4.Text;

                    this.Close();
                    FilterReturn();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }


        }
    }
}
