﻿namespace vTigerReportAddin.Dialog
{
    partial class LoadFieldMappingDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblheadertext = new System.Windows.Forms.Label();
            this.lineShape1 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.lineShape3 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lineShape2 = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.lblselectfolderpath = new System.Windows.Forms.Label();
            this.Browse = new System.Windows.Forms.Button();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.lblselectmapping = new System.Windows.Forms.Label();
            this.LoadMapped = new System.Windows.Forms.ComboBox();
            this.go = new System.Windows.Forms.Button();
            this.vTigerLoadgrid = new System.Windows.Forms.DataGridView();
            this.btnUpdate = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.vTigerLoadgrid)).BeginInit();
            this.SuspendLayout();
            // 
            // lblheadertext
            // 
            this.lblheadertext.AutoSize = true;
            this.lblheadertext.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblheadertext.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblheadertext.Location = new System.Drawing.Point(200, 9);
            this.lblheadertext.Name = "lblheadertext";
            this.lblheadertext.Size = new System.Drawing.Size(236, 24);
            this.lblheadertext.TabIndex = 1;
            this.lblheadertext.Text = "Load Field Mapping Dialog";
            this.lblheadertext.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // lineShape1
            // 
            this.lineShape1.BorderWidth = 2;
            this.lineShape1.Name = "lineShape1";
            this.lineShape1.X1 = -2;
            this.lineShape1.X2 = 618;
            this.lineShape1.Y1 = 49;
            this.lineShape1.Y2 = 49;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.lineShape3,
            this.lineShape2,
            this.lineShape1});
            this.shapeContainer1.Size = new System.Drawing.Size(619, 526);
            this.shapeContainer1.TabIndex = 2;
            this.shapeContainer1.TabStop = false;
            // 
            // lineShape3
            // 
            this.lineShape3.BorderWidth = 2;
            this.lineShape3.Name = "lineShape3";
            this.lineShape3.X1 = 0;
            this.lineShape3.X2 = 620;
            this.lineShape3.Y1 = 160;
            this.lineShape3.Y2 = 160;
            // 
            // lineShape2
            // 
            this.lineShape2.BorderWidth = 2;
            this.lineShape2.Name = "lineShape2";
            this.lineShape2.X1 = 0;
            this.lineShape2.X2 = 620;
            this.lineShape2.Y1 = 106;
            this.lineShape2.Y2 = 106;
            // 
            // lblselectfolderpath
            // 
            this.lblselectfolderpath.AutoSize = true;
            this.lblselectfolderpath.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblselectfolderpath.Location = new System.Drawing.Point(31, 68);
            this.lblselectfolderpath.Name = "lblselectfolderpath";
            this.lblselectfolderpath.Size = new System.Drawing.Size(171, 20);
            this.lblselectfolderpath.TabIndex = 3;
            this.lblselectfolderpath.Text = "Select the Folder Path:";
            // 
            // Browse
            // 
            this.Browse.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Browse.Location = new System.Drawing.Point(491, 65);
            this.Browse.Name = "Browse";
            this.Browse.Size = new System.Drawing.Size(102, 29);
            this.Browse.TabIndex = 4;
            this.Browse.Text = "Browse";
            this.Browse.UseVisualStyleBackColor = true;
            this.Browse.Click += new System.EventHandler(this.Browse_Click);
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(233, 65);
            this.txtPath.Multiline = true;
            this.txtPath.Name = "txtPath";
            this.txtPath.Size = new System.Drawing.Size(239, 29);
            this.txtPath.TabIndex = 5;
            // 
            // lblselectmapping
            // 
            this.lblselectmapping.AutoSize = true;
            this.lblselectmapping.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblselectmapping.Location = new System.Drawing.Point(35, 124);
            this.lblselectmapping.Name = "lblselectmapping";
            this.lblselectmapping.Size = new System.Drawing.Size(119, 20);
            this.lblselectmapping.TabIndex = 6;
            this.lblselectmapping.Text = "Select Mapping";
            // 
            // LoadMapped
            // 
            this.LoadMapped.FormattingEnabled = true;
            this.LoadMapped.Location = new System.Drawing.Point(233, 124);
            this.LoadMapped.Name = "LoadMapped";
            this.LoadMapped.Size = new System.Drawing.Size(143, 21);
            this.LoadMapped.TabIndex = 7;
            // 
            // go
            // 
            this.go.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.go.Location = new System.Drawing.Point(491, 115);
            this.go.Name = "go";
            this.go.Size = new System.Drawing.Size(99, 29);
            this.go.TabIndex = 8;
            this.go.Text = "Go";
            this.go.UseVisualStyleBackColor = true;
            this.go.Click += new System.EventHandler(this.go_Click);
            // 
            // vTigerLoadgrid
            // 
            this.vTigerLoadgrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.vTigerLoadgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.vTigerLoadgrid.Location = new System.Drawing.Point(29, 179);
            this.vTigerLoadgrid.Name = "vTigerLoadgrid";
            this.vTigerLoadgrid.Size = new System.Drawing.Size(561, 257);
            this.vTigerLoadgrid.TabIndex = 9;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(29, 459);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(104, 29);
            this.btnUpdate.TabIndex = 10;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // LoadFieldMappingDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(619, 526);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.vTigerLoadgrid);
            this.Controls.Add(this.go);
            this.Controls.Add(this.LoadMapped);
            this.Controls.Add(this.lblselectmapping);
            this.Controls.Add(this.txtPath);
            this.Controls.Add(this.Browse);
            this.Controls.Add(this.lblselectfolderpath);
            this.Controls.Add(this.lblheadertext);
            this.Controls.Add(this.shapeContainer1);
            this.Name = "LoadFieldMappingDialog";
            this.Text = "LoadFieldMappingDialog";
            ((System.ComponentModel.ISupportInitialize)(this.vTigerLoadgrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblheadertext;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape1;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private System.Windows.Forms.Label lblselectfolderpath;
        private System.Windows.Forms.Button Browse;
        private System.Windows.Forms.TextBox txtPath;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape2;
        private System.Windows.Forms.Label lblselectmapping;
        private System.Windows.Forms.ComboBox LoadMapped;
        private System.Windows.Forms.Button go;
        private Microsoft.VisualBasic.PowerPacks.LineShape lineShape3;
        private System.Windows.Forms.DataGridView vTigerLoadgrid;
        private System.Windows.Forms.Button btnUpdate;
    }
}