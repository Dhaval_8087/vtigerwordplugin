﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.IsolatedStorage;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using VtigerComm.Data;
using vTigerReportAddin.Models;
namespace vTigerReportAddin.Dialog
{
    public partial class FieldMappingDialog : Form
    {
        public string PrgrossTitle { get; set; }
        public FieldMappingDialog()
        {
            InitializeComponent();
            MaximizeBox = false;
            MinimizeBox = false;
            this.Load += FieldMappingDialog_Load;
            vTigerAccount.DataSource = Globals.ThisAddIn.BussinessData.vTigerModules.Select(p => p.Name).ToList();
            mergefieldcombo.DataSource = Globals.ThisAddIn.BussinessData.vTigerAccounts.Select(p => p.AccountName).ToList();
            vtigercombo.DataSource = Globals.ThisAddIn.BussinessData.vTigerAccounts.Select(p => p.AccountName).ToList();
            mergefieldcombo.SelectedIndex = 0;
            vtigercombo.SelectedIndex = 0;
            vTigerAccount.SelectedIndex = 0;
        }

        void FieldMappingDialog_Load(object sender, EventArgs e)
        {

            #region
            //var ActiveDocument = Globals.ThisAddIn.Application.ActiveDocument;

            //var Fields = ActiveDocument.Paragraphs;

            //if (Fields.Count <= 1)
            //{
            //    MessageBox.Show("You haven't enter any field to map");
            //    return;
            //}

            //foreach (Microsoft.Office.Interop.Word.Paragraph item in ActiveDocument.Paragraphs)
            //{
            //    if(item.Range.Text.Length>5)
            //    {
            //        if (item.Range.Text.ToString().Contains("«"))
            //        {
            //            var userfield = item.Range.Text.ToString().Substring(item.Range.Text.ToString().IndexOf("«") + 1, item.Range.Text.ToString().IndexOf("»") - 1);
            //            Globals.ThisAddIn.BussinessData.UserEnterField.Add(userfield);
            //        }
            //    }

            //}
            //DataGridViewCheckBoxColumn doWork = new DataGridViewCheckBoxColumn();
            //doWork.Name = "Mapping";

            //doWork.HeaderText = "Mapping";
            //doWork.FalseValue = 0;
            //doWork.TrueValue = 1;
            //vtigergrid.Columns.Insert(0, doWork);

            //DataTable dt = new DataTable();
            //dt.Columns.Add("Merge Field");
            //dt.Columns.Add("Module");
            //dt.Columns.Add("vTiger Field");

            //foreach (var item in Globals.ThisAddIn.BussinessData.vTigerAccounts)
            //{
            //    DataRow row = dt.NewRow();
            //    row["Merge Field"] = string.Empty;
            //    row["Module"] = "Account";
            //    row["vTiger Field"] = item.AccountName;
            //    dt.Rows.Add(row);

            //}

            //int i=0;
            //foreach (var item in Globals.ThisAddIn.BussinessData.UserEnterField)
            //{
            //    dt.Rows[i]["Merge Field"] = item;
            //    i++;
            //}
            //dt.AcceptChanges();
            //vtigergrid.DataSource = dt;
            #endregion

            SetCulture();
        }
        private void SetCulture()
        {
            CultureChange chang = new CultureChange("FieldMappinDialog", Globals.ThisAddIn.BussinessData.CurrentLanguage);
            this.Text = chang.getValue("Title");
            lblheadertext.Text = chang.getValue("Headertext");
            lbltitle.Text = chang.getValue("lbltitle");
            lbldescription.Text = chang.getValue("description");
            lblmodule.Text = chang.getValue("Module");
            lblmergefield.Text = chang.getValue("mergefield");
            lblvtigerield.Text = chang.getValue("vtigerfieldss");
            button1.Text = chang.getValue("add");
            button3.Text = chang.getValue("save");
            button4.Text = chang.getValue("cancel");
            PrgrossTitle = chang.getValue("ProgressbarTitle");
        }

        private List<GridItems> _gridItems = new List<GridItems>();

        public List<GridItems> gridItems
        {
            get { return _gridItems; }
            set { _gridItems = value; }
        }

        private void Add_Click(object sender, EventArgs e)
        {
            try
            {
                GridItems gi = new GridItems();
                gi.Mergefield = mergefieldcombo.Text.ToString();
                gi.vTigerfield = vtigercombo.Text.ToString();
                gi.vAccount = vTigerAccount.Text.ToString(); ;
                gridItems.Add(gi);

                DataGridViewCheckBoxColumn doWork = new DataGridViewCheckBoxColumn();
                doWork.Name = "Mapping";

                doWork.HeaderText = "Mapping";
                doWork.FalseValue = 0;
                doWork.TrueValue = 1;
                //doWork.IndeterminateValue = true;

                //vtigergrid.Columns.Insert(0, doWork);

                DataTable dt = new DataTable();

                dt.Columns.Add(new DataColumn("Mapping", typeof(bool)));
                dt.Columns.Add("Merge Field");
                dt.Columns.Add("Module");
                dt.Columns.Add("vTiger Field");

                foreach (var item in gridItems)
                {
                    DataRow row = dt.NewRow();
                    row["Mapping"] = true;
                    row["Merge Field"] = item.Mergefield;
                    row["Module"] = item.vAccount;
                    row["vTiger Field"] = item.vTigerfield;
                    dt.Rows.Add(row);
                }

                dt.AcceptChanges();

                vtigergrid.AllowUserToAddRows = false;
                vtigergrid.DataSource = dt;
            }
            catch (Exception ex)
            {

                MessageBox.Show("Oppss!!!! " + ex.Message);
            }


        }



        private void Save_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtTitle.Text))
                {
                    MessageBox.Show("Please enter Title");
                    return;
                }
                XElement docelement = new XElement("Documents");
                XElement titleElemnt = new XElement("Title");
                titleElemnt.Value = txtTitle.Text;
                XElement Descriptionele = new XElement("Description");
                Descriptionele.Value = txtDescription.Text;
                docelement.Add(titleElemnt);
                docelement.Add(Descriptionele);

                foreach (DataGridViewRow row in vtigergrid.Rows)
                {
                    var Mapped = Convert.ToBoolean(row.Cells["Mapping"].Value);
                    if (Mapped == true)
                    {
                        XElement eleMappings = new XElement("Mappings");

                        XElement vMergeField = new XElement("vMergeField");
                        vMergeField.Value = row.Cells["Merge Field"].Value.ToString();

                        XElement vModule = new XElement("vModule");
                        vModule.Value = row.Cells["Module"].Value.ToString();

                        XElement vtigerField = new XElement("vTigerField");
                        vtigerField.Value = row.Cells["vTiger Field"].Value.ToString();

                        eleMappings.Add(vMergeField);
                        eleMappings.Add(vModule);
                        eleMappings.Add(vtigerField);
                        docelement.Add(eleMappings);
                    }



                }

                string FileName = txtTitle.Text + ".xml";

                SaveFileDialog oled = new SaveFileDialog();
                oled.Filter = "XML Files (*.xml)|*.xml";
                if (oled.ShowDialog() == DialogResult.OK)
                {
                    var FilePath = oled.FileName;
                    StreamWriter writer = new StreamWriter(FilePath);
                    writer.Write(docelement);
                    writer.Close();
                    MessageBox.Show("Field Mapping Saved !!!");
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show("Oppss!!!! " + ex.Message);
            }


        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Do You Want to Cancel it?", "Delete", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (result.Equals(DialogResult.OK))
            {
                vtigergrid.DataSource = new DataTable();
                gridItems = new List<GridItems>();
                vTigerAccount.Enabled = true;
            }
            

            //this.Close();
        }

        private void vTigerAccount_SelectionChangeCommitted(object sender, EventArgs e)
        {
            vTigerAccount.Enabled = false;
            try
            {
                // Initialize the dialog that will contain the progress bar
                ProgressDialog progressDialog = new ProgressDialog();
                progressDialog.Text = PrgrossTitle;
                string Value = vTigerAccount.SelectedValue.ToString();
                // Initialize the thread that will handle the background process
                Thread backgroundThread = new Thread(
                    new ThreadStart(() =>
                    {
                        // Set the flag that indicates if a process is currently running

                        progressDialog.SetIndeterminate(true);
                        VTEntity vte = InfoService.getAllFieldsFromEntity(Globals.ThisAddIn.BussinessData.User, Value);
                        fillCombo(vte);

                        // Close the dialog if it hasn't been already
                        if (progressDialog.InvokeRequired)
                            progressDialog.BeginInvoke(new Action(() => progressDialog.Close()));


                    }
                ));

                // Start the background process thread
                backgroundThread.Start();

                // Open the dialog
                progressDialog.ShowDialog();
            }
            catch (Exception ex)
            {

                MessageBox.Show("Oppss!!!! " + ex.Message);
            }


        }

        private void fillCombo(VTEntity vte)
        {
            List<string> Entity = new List<string>();
            foreach (KeyValuePair<string, Field> elem in vte.Fields)
            {
                Entity.Add(elem.Key.ToString());
            }
            Invoke(new Action(() =>
            {
                vtigercombo.DataSource = Entity;

                mergefieldcombo.DataSource = new List<string>(Entity);
            }));


        }

        private void mergefieldcombo_TextChanged(object sender, EventArgs e)
        { 
        }
    }
}
