﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using VtigerComm.Data;

namespace vTigerReportAddin.Dialog
{
    public partial class ReportGenerationdialog : Form
    {
        DataTable dt = new DataTable();
        DataTable dto = new DataTable();
        public ReportGenerationdialog()
        {
            InitializeComponent();
            MaximizeBox = false;
            MinimizeBox = false;
            vTigerLoadgrid.AllowUserToAddRows = false;
        }

        private void Browse_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog();
                DialogResult result = dialog.ShowDialog();

                dt = new DataTable();
                if (result == DialogResult.OK)
                {
                    if (Path.GetExtension(dialog.FileName) == ".xml")
                    {
                        txtPath.Text = dialog.FileName;
                        dt.Columns.Add(new DataColumn("Mapping", typeof(bool)));

                        // Initialize the dialog that will contain the progress bar
                        ProgressDialog progressDialog = new ProgressDialog();
                        // Initialize the thread that will handle the background process
                        Thread backgroundThread = new Thread(
                            new ThreadStart(() =>
                            {
                                // Set the flag that indicates if a process is currently running

                                progressDialog.SetIndeterminate(true);

                                DataSet ds = new DataSet();
                                ds.ReadXml(dialog.FileName.ToString());

                                var Module = ds.Tables[1].Rows[0]["vModule"].ToString();
                                var Query = "Select * from " + Module;
                                List<VTEntity> lst = VtigerComm.VtigerCRUD.QueryEntities(Query, Globals.ThisAddIn.BussinessData.User);
                                FillDataGrid(dialog, ds, lst);

                                // Close the dialog if it hasn't been already
                                if (progressDialog.InvokeRequired)
                                    progressDialog.BeginInvoke(new Action(() => progressDialog.Close()));


                            }
                        ));

                        // Start the background process thread
                        backgroundThread.Start();

                        // Open the dialog
                        progressDialog.ShowDialog();

                    }
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }


        }

        private void FillDataGrid(OpenFileDialog dialog, DataSet ds, List<VTEntity> lst)
        {
            Invoke(new Action(() =>
            {

                var Fields = lst.Select(p => p.Fields.Keys);
                List<Entityame> DataWithValue = new List<Entityame>();
                var MergedFields = ds.Tables[1].AsEnumerable().Select(p => p.Field<string>("vMergeField"));
                foreach (VTEntity elem in lst)
                {
                    foreach (KeyValuePair<string, Field> inner in elem.Fields)
                    {
                        if (MergedFields.Any(p => p.ToString() == inner.Key) && dt.Columns.Contains(inner.Key) == false)
                        {
                            dt.Columns.Add(inner.Key);
                        }

                    }

                }
                foreach (VTEntity elem in lst)
                {
                    DataRow row = dt.NewRow();
                    row["Mapping"] = true;
                    foreach (KeyValuePair<string, Field> inner in elem.Fields)
                    {
                        if (MergedFields.Any(p => p.ToString() == inner.Key))
                        {
                            row[inner.Key] = inner.Value;
                        }

                    }
                    dt.Rows.Add(row);


                }


                vTigerLoadgrid.DataSource = dt;
                dto = dt.Copy();
            }));

        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            //dt.DefaultView.RowFilter = "ItemCode like '%" + txtsearch.Text + "%' or ItemsDescription like  '%" + txtsearch.Text + "%'";
            // grdmeal.ItemsSource = dt.DefaultView;
        }

        private void filter_Click(object sender, EventArgs e)
        {
            FilterDialog filter = new FilterDialog();
            filter.FilterReturn += filter_FilterReturn;
            filter.Show();

        }

        void filter_FilterReturn()
        {
            // dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilderField + " like '%" + Globals.ThisAddIn.BussinessData.FilterValue + "%' or ItemsDescription like  '%" + txtsearch.Text + "%'";
            if (dt.Rows.Count > 0)
            {
                if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation).FirstOrDefault().Value == 0)
                    dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " like '%" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "%'";
                else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation).FirstOrDefault().Value == 1)
                    dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " <> '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation).FirstOrDefault().Value == 2)
                    dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " >= '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation).FirstOrDefault().Value == 3)
                    dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " > '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation).FirstOrDefault().Value == 4)
                    dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " <= '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation).FirstOrDefault().Value == 5)
                    dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " < '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation).FirstOrDefault().Value == 6)
                    dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " IS NULL AND " + Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " = ''";


                if (string.IsNullOrEmpty(Globals.ThisAddIn.BussinessData.FilterProperty.FilderField1) == false)
                {
                    if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation1).FirstOrDefault().Value == 0)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " like '%" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "%'";
                    else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation1).FirstOrDefault().Value == 1)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " <> '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                    else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation1).FirstOrDefault().Value == 2)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " >= '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                    else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation1).FirstOrDefault().Value == 3)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " > '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                    else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation1).FirstOrDefault().Value == 4)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " <= '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                    else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation1).FirstOrDefault().Value == 5)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " < '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                    else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation1).FirstOrDefault().Value == 6)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " IS NULL AND " + Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " = ''";

                }

                if (string.IsNullOrEmpty(Globals.ThisAddIn.BussinessData.FilterProperty.FilderField2) == false)
                {
                    if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation2).FirstOrDefault().Value == 0)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " like '%" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "%'";
                    else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation2).FirstOrDefault().Value == 1)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " <> '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                    else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation2).FirstOrDefault().Value == 2)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " >= '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                    else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation2).FirstOrDefault().Value == 3)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " > '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                    else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation2).FirstOrDefault().Value == 4)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " <= '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                    else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation2).FirstOrDefault().Value == 5)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " < '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                    else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation2).FirstOrDefault().Value == 6)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " IS NULL AND " + Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " = ''";

                }

                if (string.IsNullOrEmpty(Globals.ThisAddIn.BussinessData.FilterProperty.FilderField3) == false)
                {
                    if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation3).FirstOrDefault().Value == 0)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " like '%" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "%'";
                    else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation3).FirstOrDefault().Value == 1)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " <> '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                    else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation3).FirstOrDefault().Value == 2)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " >= '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                    else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation3).FirstOrDefault().Value == 3)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " > '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                    else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation3).FirstOrDefault().Value == 4)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " <= '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                    else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation3).FirstOrDefault().Value == 5)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " < '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                    else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation3).FirstOrDefault().Value == 6)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " IS NULL AND " + Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " = ''";

                }

                if (string.IsNullOrEmpty(Globals.ThisAddIn.BussinessData.FilterProperty.FilderField4) == false)
                {
                    if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation4).FirstOrDefault().Value == 0)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " like '%" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "%'";
                    else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation4).FirstOrDefault().Value == 1)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " <> '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                    else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation4).FirstOrDefault().Value == 2)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " >= '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                    else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation4).FirstOrDefault().Value == 3)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " > '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                    else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation4).FirstOrDefault().Value == 4)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " <= '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                    else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation4).FirstOrDefault().Value == 5)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " < '" + Globals.ThisAddIn.BussinessData.FilterProperty.FilterValue + "'";

                    else if (Globals.ThisAddIn.BussinessData.ComparisionCollection.Where(p => p.Name == Globals.ThisAddIn.BussinessData.FilterProperty.FilterOperation4).FirstOrDefault().Value == 6)
                        dt.DefaultView.RowFilter = Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " IS NULL AND " + Globals.ThisAddIn.BussinessData.FilterProperty.FilderField + " = ''";

                }

                vTigerLoadgrid.DataSource = dt.DefaultView;
            }

        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            vTigerLoadgrid.DataSource = dto;

        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            var ActiveDocument = Globals.ThisAddIn.Application.ActiveDocument;
            foreach (Microsoft.Office.Interop.Word.Table tbl in ActiveDocument.Tables)
            {
                tbl.Delete();

            }
            int noofrows = 0;
            int noocol = 0;
            foreach (DataGridViewRow row in vTigerLoadgrid.Rows)
            {
                var Mapped = Convert.ToBoolean(row.Cells["Mapping"].Value);
                if (Mapped)
                {
                    noofrows++;
                }
            }
            foreach (var item in vTigerLoadgrid.Columns)
            {
                if (item.GetType() == typeof(DataGridViewTextBoxColumn))
                {
                    noocol++;
                }
            }
            var table = ActiveDocument.Tables.Add(Globals.ThisAddIn.Application.ActiveDocument.Range(0, 0), noofrows + 1, noocol);
            ActiveDocument.Tables[1].Range.Shading.BackgroundPatternColor = Microsoft.Office.Interop.Word.WdColor.wdColorGray125;
            ActiveDocument.Tables[1].Range.Font.Size = 12;
            ActiveDocument.Tables[1].Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
            ActiveDocument.Tables[1].Rows.Borders.Enable = 1;
            //table.Cell(1, 1).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
            //table.Cell(1, 1).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
            //table.Cell(1, 1).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
            //table.Cell(1, 1).Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
            //table.Cell(1, 1).Range.Text = "Module";
            //table.Cell(1, 1).Range.Text = "Mapping Field";
            //table.Cell(1, 2).Range.Text = "Value";
            int Col = 1;
            var colo = vTigerLoadgrid.Columns;
            foreach (var item in vTigerLoadgrid.Columns)
            {
                if (item.GetType() == typeof(DataGridViewTextBoxColumn))
                {
                    table.Cell(1, Col).Range.Text = (item as DataGridViewTextBoxColumn).HeaderText.ToString();
                    Col++;
                }
            }
            int DocRow = 2;

            foreach (DataGridViewRow row in vTigerLoadgrid.Rows)
            {
                var Mapped = Convert.ToBoolean(row.Cells["Mapping"].Value);
                if (Mapped)
                {
                    for (int i = 1; i <= noocol; i++)
                    {
                        table.Cell(DocRow, i).Range.Text = row.Cells[i].Value.ToString();
                    }


                }
                DocRow++;
            }
            //Row = 2;
            //foreach (DataGridViewRow row in vTigerLoadgrid.Rows)
            //{
            //    var Mapped = Convert.ToBoolean(row.Cells["Mapping"].Value);
            //    if (Mapped == true)
            //    {
            //        table.Cell(Row, 2).Range.Text = row.Cells[2].Value.ToString();
            //        //  table.Cell(Row, 2).Range.Text = row.Cells[1].Value.ToString();

            //    }
            //    Row++;
            //}
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
    public class Entityame
    {
        public string FieldName { get; set; }
        public string FieldValue { get; set; }
    }
}
