﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Word;
using vTigerReportAddin.Models;

namespace vTigerReportAddin
{
    public partial class ThisAddIn
    {
        private BusinessClass _BussinessData;

        public BusinessClass BussinessData
        {
            get { return _BussinessData ?? (_BussinessData = new BusinessClass()); }
            set { _BussinessData = value; }
        }
       
        
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            this.Application.MailMergeAfterMerge += Application_MailMergeAfterMerge;
        }

        void Application_MailMergeAfterMerge(Word.Document Doc, Word.Document DocResult)
        {
            
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
